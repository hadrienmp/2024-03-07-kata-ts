import { Shop } from "./gildedRose"
import { Item } from "./Item"
import { configure } from "approvals/lib/Approvals"
import { JestReporter } from "approvals/lib/Providers/Jest/JestReporter"
import { verifyAllCombinations3 } from "approvals/lib/Providers/Jest/CombinationApprovals"

describe("Gilded Rose", () => {
  beforeAll(() => {
    configure({
      reporters: [new JestReporter()],
    })
  })

  it("test de la ligne 14", () => {
    const shop = new Shop()
    const updatedItems = shop.updateQuality()
    expect(updatedItems).toEqual([])
  })

  it("asdasdf", () => {
    const shop = new Shop([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 50)])
    const updatedItems = shop.updateQuality()
    expect(updatedItems).toEqual([new Item('Backstage passes to a TAFKAL80ETC concert', -1, 0)])
  })
  it("remplacer par des trucs de droite", () => {
    verifyAllCombinations3(
      (name, sellIn, quality) => {
        const items = [new Item(name, sellIn, quality)]
        const shop = new Shop(items)
        const updatedItems = shop.updateQuality()
        return JSON.stringify(updatedItems)
      },
      [
        "Hadrien",
        "Pin",
        "Manon",
        "Aged Brie",
        "Backstage passes to a TAFKAL80ETC concert",
        "Sulfuras, Hand of Ragnaros",
      ],
      [-10, -9, -4, -2, -1, 0, 1, 5, 6, 7, 10, 11, 12, 33, 42, 100],
      [-10, -9, -4, -2, -1, 0, 1, 25, 49, 50, 51, 74, 100, 1312]
    )
  })
})


it("asadfsfsdasdf", () => {
  const shop = new Shop([new Item('Backstage passes to a TAFKAL80ETC concert', 100, 49)])
  const updatedItems = shop.updateQuality()
  expect(updatedItems).toEqual([new Item('Backstage passes to a TAFKAL80ETC concert', 99, 50)])
})