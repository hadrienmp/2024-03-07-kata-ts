import { Item } from "./Item"

export class Shop {
  items: Item[]
  constructor(items: Item[] = []) {
    this.items = items
  }

  updateQuality() {
    this.items = this.items.map(item => {
      switch (item.name) {
        case "Aged Brie":
          return this.updateAgedBrie(item)
        case "Backstage passes to a TAFKAL80ETC concert":
          return updateBackstagePasses(item)
        default:
          updateLol(item)
          return item
      }

    })
    return this.items
  }

  private updateAgedBrie(item: Item): Item {
    return updateAgedBrieQuality(decrementSellIn(item))
  }
}

const decrementSellIn = (item: Item): Item => new Item(
  item.name,
  item.sellIn - 1,
  item.quality
)


const withQuality = (newQuality: number) => (item: Item): Item => new Item(
  item.name,
  item.sellIn,
  newQuality > 50 ? Math.max(50, item.quality) : Math.min(50, newQuality)
)

const updateAgedBrieQuality = (first: Item): Item => withQuality(first.quality + (first.sellIn >= 0 ? 1 : 2))(first)

function updateLol(item: Item) {
  if (item.quality > 0) {
    if (item.name != "Sulfuras, Hand of Ragnaros") {
      item.quality--
    }
  }
  if (item.name != "Sulfuras, Hand of Ragnaros") {
    item.sellIn--
  }
  if (item.sellIn < 0) {
    if (item.quality > 0) {
      if (item.name != "Sulfuras, Hand of Ragnaros") {
        item.quality--
      }
    }
  }
}


function updateBackstagePasses(item: Item): Item {
  return nextBackstagePassesQuality(decrementSellIn(item))
}

function nextBackstagePassesQuality(item: Item): Item {
  switch (true) {
    case item.sellIn < 0: return cestPlusBon(item)
    case item.sellIn < 5: return addQuality(item, 3)
    case item.sellIn < 10: return addQuality(item, 2)
    default: return addQuality(item, 1)
  }
}

function cestPlusBon(item: Item): Item {
  return withQuality(0)(item)
}

function addQuality(item: Item, delta: number): Item {
  return withQuality(item.quality + delta)(item)
}
